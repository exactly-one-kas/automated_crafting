---- Minecraft Crash Report ----
// Don't be sad, have a hug! <3

Time: 9/1/20 4:36 PM
Description: Exception while ticking

java.lang.IllegalArgumentException: Cannot set property BooleanProperty{name=activated, clazz=class java.lang.Boolean, values=[true, false]} as it does not exist in Block{automated_crafting:auto_crafter}
	at net.minecraft.state.State.with(State.java:114)
	at net.sssubtlety.automated_crafting.block.AutoCrafterBlock.scheduledTick(AutoCrafterBlock.java:95)
	at net.minecraft.block.AbstractBlock$AbstractBlockState.scheduledTick(AbstractBlock.java:874)
	at net.minecraft.server.world.ServerWorld.tickBlock(ServerWorld.java:597)
	at net.minecraft.server.world.ServerTickScheduler.tick(ServerTickScheduler.java:84)
	at net.minecraft.server.world.ServerWorld.tick(ServerWorld.java:352)
	at net.minecraft.server.MinecraftServer.tickWorlds(MinecraftServer.java:868)
	at net.minecraft.server.MinecraftServer.tick(MinecraftServer.java:808)
	at net.minecraft.server.integrated.IntegratedServer.tick(IntegratedServer.java:92)
	at net.minecraft.server.MinecraftServer.runServer(MinecraftServer.java:667)
	at net.minecraft.server.MinecraftServer.method_29739(MinecraftServer.java:254)
	at java.lang.Thread.run(Thread.java:748)


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- Head --
Thread: Server thread
Stacktrace:
	at net.minecraft.state.State.with(State.java:114)
	at net.sssubtlety.automated_crafting.block.AutoCrafterBlock.scheduledTick(AutoCrafterBlock.java:95)
	at net.minecraft.block.AbstractBlock$AbstractBlockState.scheduledTick(AbstractBlock.java:874)
	at net.minecraft.server.world.ServerWorld.tickBlock(ServerWorld.java:597)

-- Block being ticked --
Details:
	Block location: World: (102,71,220), Chunk: (at 6,4,12 in 6,13; contains blocks 96,0,208 to 111,255,223), Region: (0,0; contains chunks 0,0 to 31,31, blocks 0,0,0 to 511,255,511)
Stacktrace:
	at net.minecraft.server.world.ServerTickScheduler.tick(ServerTickScheduler.java:84)
	at net.minecraft.server.world.ServerWorld.tick(ServerWorld.java:352)

-- Affected level --
Details:
	All players: 1 total; [ServerPlayerEntity['Player835'/243, l='ServerLevel[1.16.2]', x=100.89, y=71.00, z=222.23]]
	Chunk stats: ServerChunkCache: 2025
	Level dimension: minecraft:overworld
	Level spawn location: World: (96,71,224), Chunk: (at 0,4,0 in 6,14; contains blocks 96,0,224 to 111,255,239), Region: (0,0; contains chunks 0,0 to 31,31, blocks 0,0,0 to 511,255,511)
	Level time: 237677 game time, 1145 day time
	Level name: 1.16.2
	Level game mode: Game mode: creative (ID 1). Hardcore: false. Cheats: true
	Level weather: Rain time: 18683 (now: false), thunder time: 120628 (now: false)
	Known server brands: fabric
	Level was modded: true
	Level storage version: 0x04ABD - Anvil
Stacktrace:
	at net.minecraft.server.MinecraftServer.tickWorlds(MinecraftServer.java:868)
	at net.minecraft.server.MinecraftServer.tick(MinecraftServer.java:808)
	at net.minecraft.server.integrated.IntegratedServer.tick(IntegratedServer.java:92)
	at net.minecraft.server.MinecraftServer.runServer(MinecraftServer.java:667)
	at net.minecraft.server.MinecraftServer.method_29739(MinecraftServer.java:254)
	at java.lang.Thread.run(Thread.java:748)

-- System Details --
Details:
	Minecraft Version: 1.16.2
	Minecraft Version ID: 1.16.2
	Operating System: Windows 10 (amd64) version 10.0
	Java Version: 1.8.0_251, Oracle Corporation
	Java VM Version: Java HotSpot(TM) 64-Bit Server VM (mixed mode), Oracle Corporation
	Memory: 347752112 bytes (331 MB) / 891813888 bytes (850 MB) up to 954728448 bytes (910 MB)
	CPUs: 16
	JVM Flags: 1 total; -Xmx1G
	Fabric Mods: 
		autoconfig1u: Auto Config v1 Updated 3.2.0-unstable
		automated_crafting: Automated Crafting 1.3.8+1.16.2
		cloth-basic-math: Cloth Basic Math 0.5.1
		cloth-config2: Cloth Config v4 4.8.1
		fabric: Fabric API 0.19.0+build.398-1.16
		fabric-api-base: Fabric API Base 0.1.3+12a8474c4e
		fabric-blockrenderlayer-v1: Fabric BlockRenderLayer Registration (v1) 1.1.4+c6a8ea894e
		fabric-command-api-v1: Fabric Command API (v1) 1.0.8+5ce533984e
		fabric-commands-v0: Fabric Commands (v0) 0.2.0+52d308364e
		fabric-containers-v0: Fabric Containers (v0) 0.1.8+045df74f4e
		fabric-content-registries-v0: Fabric Content Registries (v0) 0.1.9+059ea8664e
		fabric-crash-report-info-v1: Fabric Crash Report Info (v1) 0.1.2+b7f9825d4e
		fabric-events-interaction-v0: Fabric Events Interaction (v0) 0.4.0+e2e6cdad4e
		fabric-events-lifecycle-v0: Fabric Events Lifecycle (v0) 0.2.0+16acbe5b4e
		fabric-game-rule-api-v1: Fabric Game Rule API (v1) 1.0.1+91555cd14e
		fabric-item-api-v1: Fabric Item API (v1) 1.1.0+29679fd64e
		fabric-item-groups-v0: Fabric Item Groups (v0) 0.2.0+438f96364e
		fabric-key-binding-api-v1: Fabric Key Binding API (v1) 1.0.1+730711c64e
		fabric-keybindings-v0: Fabric Key Bindings (v0) 0.2.0+3fa9f7c54e
		fabric-lifecycle-events-v1: Fabric Lifecycle Events (v1) 1.2.0+74cc3b204e
		fabric-loot-tables-v1: Fabric Loot Tables (v1) 1.0.0+fac8f3664e
		fabric-mining-levels-v0: Fabric Mining Levels (v0) 0.1.2+b764ce994e
		fabric-models-v0: Fabric Models (v0) 0.1.0+dfdb52d64e
		fabric-networking-blockentity-v0: Fabric Networking Block Entity (v0) 0.2.5+b50ffc7b4e
		fabric-networking-v0: Fabric Networking (v0) 0.1.10+e00ecb5f4e
		fabric-object-builder-api-v1: Fabric Object Builder API (v1) 1.6.0+e81ec80b4e
		fabric-object-builders-v0: Fabric Object Builders (v0) 0.6.1+ba4afa574e
		fabric-particles-v1: fabric-particles-v1 0.2.1+0a6f2a704e
		fabric-registry-sync-v0: Fabric Registry Sync (v0) 0.4.3+e5d3217f4e
		fabric-renderer-api-v1: Fabric Renderer API (v1) 0.2.13+eae12eb84e
		fabric-renderer-indigo: Fabric Renderer - Indigo 0.3.4+5d32f5834e
		fabric-renderer-registries-v1: Fabric Renderer Registries (v1) 2.1.0+e2862de64e
		fabric-rendering-data-attachment-v1: Fabric Rendering Data Attachment (v1) 0.1.3+b7f9825d4e
		fabric-rendering-fluids-v1: Fabric Rendering Fluids (v1) 0.1.10+e5d3217f4e
		fabric-rendering-v0: Fabric Rendering (v0) 1.1.0+534104904e
		fabric-rendering-v1: Fabric Rendering (v1) 1.2.0+fc24ff244e
		fabric-resource-loader-v0: Fabric Resource Loader (v0) 0.2.9+e5d3217f4e
		fabric-screen-handler-api-v1: Fabric Screen Handler API (v1) 1.1.0+872498414e
		fabric-structure-api-v1: Fabric Structure API (v1) 1.0.0+516ece7c4e
		fabric-tag-extensions-v0: Fabric Tag Extensions (v0) 1.0.3+ac8e8c594e
		fabric-textures-v0: Fabric Textures (v0) 1.0.4+eae12eb84e
		fabric-tool-attribute-api-v1: Fabric Tool Attribute API (v1) 1.2.2+ccce563e4e
		fabricloader: Fabric Loader 0.9.1+build.205
		jankson: Jankson 3.0.1+j1.2.0
		libblockattributes: LibBlockAttributes 0.8.1
		libblockattributes_core: LibBlockAttributes (Core) 0.8.1
		libblockattributes_fluids: LibBlockAttributes (Fluids) 0.8.1
		libblockattributes_items: LibBlockAttributes (Items) 0.8.1
		libgui: LibGui 3.0.0-beta.1+1.16.2-rc2
		minecraft: Minecraft 1.16.2
		modmenu: Mod Menu 1.14.6+build.31
	Player Count: 1 / 8; [ServerPlayerEntity['Player835'/243, l='ServerLevel[1.16.2]', x=100.89, y=71.00, z=222.23]]
	Data Packs: vanilla, fabric/fabric-tool-attribute-api-v1, fabric/automated_crafting
	Type: Integrated Server (map_client.txt)
	Is Modded: Definitely; Client brand changed to 'fabric'