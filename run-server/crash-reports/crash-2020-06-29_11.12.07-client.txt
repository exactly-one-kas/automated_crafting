---- Minecraft Crash Report ----
// You're mean.

Time: 6/29/20 11:12 AM
Description: Initializing game

java.lang.RuntimeException: Could not execute entrypoint stage 'client' due to errors, provided by 'modmenu'!
	at net.fabricmc.loader.entrypoint.minecraft.hooks.EntrypointUtils.invoke0(EntrypointUtils.java:53)
	at net.fabricmc.loader.entrypoint.minecraft.hooks.EntrypointUtils.invoke(EntrypointUtils.java:36)
	at net.fabricmc.loader.entrypoint.minecraft.hooks.EntrypointClient.start(EntrypointClient.java:33)
	at net.minecraft.client.MinecraftClient.<init>(MinecraftClient.java:400)
	at net.minecraft.client.main.Main.main(Main.java:149)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at net.fabricmc.loader.game.MinecraftGameProvider.launch(MinecraftGameProvider.java:192)
	at net.fabricmc.loader.launch.knot.Knot.init(Knot.java:140)
	at net.fabricmc.loader.launch.knot.KnotClient.main(KnotClient.java:26)
	at net.fabricmc.devlaunchinjector.Main.main(Main.java:86)
Caused by: net.fabricmc.loader.api.EntrypointException: Exception while loading entries for entrypoint 'modmenu' provided by 'automated_crafting'
	at net.fabricmc.loader.EntrypointStorage.getEntrypoints(EntrypointStorage.java:163)
	at net.fabricmc.loader.FabricLoader.getEntrypoints(FabricLoader.java:223)
	at io.github.prospector.modmenu.ModMenu.onInitializeClient(ModMenu.java:68)
	at net.fabricmc.loader.entrypoint.minecraft.hooks.EntrypointUtils.invoke0(EntrypointUtils.java:50)
	... 12 more
Caused by: net.fabricmc.loader.api.LanguageAdapterException: java.lang.ClassNotFoundException: net.sssubtlety.automated_crafting.AutomatedCraftingModMenuIntegration
	at net.fabricmc.loader.util.DefaultLanguageAdapter.create(DefaultLanguageAdapter.java:47)
	at net.fabricmc.loader.EntrypointStorage$NewEntry.create(EntrypointStorage.java:114)
	at net.fabricmc.loader.EntrypointStorage$NewEntry.getOrCreate(EntrypointStorage.java:101)
	at net.fabricmc.loader.EntrypointStorage.getEntrypoints(EntrypointStorage.java:156)
	... 15 more
Caused by: java.lang.ClassNotFoundException: net.sssubtlety.automated_crafting.AutomatedCraftingModMenuIntegration
	at java.net.URLClassLoader.findClass(URLClassLoader.java:382)
	at java.lang.ClassLoader.loadClass(ClassLoader.java:418)
	at sun.misc.Launcher$AppClassLoader.loadClass(Launcher.java:355)
	at java.lang.ClassLoader.loadClass(ClassLoader.java:351)
	at net.fabricmc.loader.launch.knot.KnotClassLoader.loadClass(KnotClassLoader.java:161)
	at java.lang.ClassLoader.loadClass(ClassLoader.java:351)
	at java.lang.Class.forName0(Native Method)
	at java.lang.Class.forName(Class.java:348)
	at net.fabricmc.loader.util.DefaultLanguageAdapter.create(DefaultLanguageAdapter.java:45)
	... 18 more


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- Head --
Thread: Render thread
Stacktrace:
	at net.fabricmc.loader.entrypoint.minecraft.hooks.EntrypointUtils.invoke0(EntrypointUtils.java:53)
	at net.fabricmc.loader.entrypoint.minecraft.hooks.EntrypointUtils.invoke(EntrypointUtils.java:36)
	at net.fabricmc.loader.entrypoint.minecraft.hooks.EntrypointClient.start(EntrypointClient.java:33)
	at net.minecraft.client.MinecraftClient.<init>(MinecraftClient.java:400)

-- Initialization --
Details:
Stacktrace:
	at net.minecraft.client.main.Main.main(Main.java:149)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at net.fabricmc.loader.game.MinecraftGameProvider.launch(MinecraftGameProvider.java:192)
	at net.fabricmc.loader.launch.knot.Knot.init(Knot.java:140)
	at net.fabricmc.loader.launch.knot.KnotClient.main(KnotClient.java:26)
	at net.fabricmc.devlaunchinjector.Main.main(Main.java:86)

-- System Details --
Details:
	Minecraft Version: 1.16.1
	Minecraft Version ID: 1.16.1
	Operating System: Windows 10 (amd64) version 10.0
	Java Version: 1.8.0_251, Oracle Corporation
	Java VM Version: Java HotSpot(TM) 64-Bit Server VM (mixed mode), Oracle Corporation
	Memory: 585889312 bytes (558 MB) / 824180736 bytes (786 MB) up to 954728448 bytes (910 MB)
	CPUs: 16
	JVM Flags: 1 total; -Xmx1G
	Fabric Mods: 
		autoconfig1u: Auto Config v1 Updated 3.2.0-unstable
		automated_crafting: Automated Crafting 1.0.0+1.16.1
		cloth-basic-math: Cloth Basic Math 0.5.1
		cloth-config2: Cloth Config v4 4.5.6
		fabric: Fabric API 0.13.1+build.370-1.16
		fabric-api-base: Fabric API Base 0.1.3+12a8474c45
		fabric-biomes-v1: Fabric Biomes (v1) 0.2.7+059ea86645
		fabric-blockrenderlayer-v1: Fabric BlockRenderLayer Registration (v1) 1.1.4+c6a8ea8945
		fabric-command-api-v1: Fabric Command API (v1) 1.0.8+5ce533987c
		fabric-commands-v0: Fabric Commands (v0) 0.2.0+52d3083645
		fabric-containers-v0: Fabric Containers (v0) 0.1.8+045df74f45
		fabric-content-registries-v0: Fabric Content Registries (v0) 0.1.9+059ea86645
		fabric-crash-report-info-v1: Fabric Crash Report Info (v1) 0.1.2+b7f9825d45
		fabric-dimensions-v1: fabric-dimensions-v1 1.0.0+a71b305345
		fabric-events-interaction-v0: Fabric Events Interaction (v0) 0.3.3+7066030f45
		fabric-events-lifecycle-v0: Fabric Events Lifecycle (v0) 0.1.3+0d474ec445
		fabric-item-groups-v0: Fabric Item Groups (v0) 0.2.0+438f963645
		fabric-key-binding-api-v1: Fabric Key Binding API (v1) 1.0.0+e16a977445
		fabric-keybindings-v0: Fabric Key Bindings (v0) 0.2.0+3fa9f7c545
		fabric-loot-tables-v1: Fabric Loot Tables (v1) 0.1.10+059ea86645
		fabric-mining-levels-v0: Fabric Mining Levels (v0) 0.1.2+b764ce9945
		fabric-models-v0: Fabric Models (v0) 0.1.0+dfdb52d645
		fabric-networking-blockentity-v0: Fabric Networking Block Entity (v0) 0.2.5+b50ffc7b45
		fabric-networking-v0: Fabric Networking (v0) 0.1.10+e00ecb5f7c
		fabric-object-builder-api-v1: Fabric Object Builder API (v1) 1.5.5+e00ecb5f7c
		fabric-object-builders-v0: Fabric Object Builders (v0) 0.6.0+da175ad645
		fabric-particles-v1: fabric-particles-v1 0.2.1+0a6f2a707c
		fabric-registry-sync-v0: Fabric Registry Sync (v0) 0.3.8+7dba2d6c45
		fabric-renderer-api-v1: Fabric Renderer API (v1) 0.2.13+eae12eb845
		fabric-renderer-indigo: Fabric Renderer - Indigo 0.3.1+059ea86645
		fabric-renderer-registries-v1: Fabric Renderer Registries (v1) 2.0.1+5a0f9a6045
		fabric-rendering-data-attachment-v1: Fabric Rendering Data Attachment (v1) 0.1.3+b7f9825d45
		fabric-rendering-fluids-v1: Fabric Rendering Fluids (v1) 0.1.7+12a8474c45
		fabric-rendering-v0: Fabric Rendering (v0) 1.1.0+5341049045
		fabric-rendering-v1: Fabric Rendering (v1) 1.1.2+346247d745
		fabric-resource-loader-v0: Fabric Resource Loader (v0) 0.2.5+059ea86645
		fabric-screen-handler-api-v1: Fabric Screen Handler API (v1) 1.0.1+f362c86e7c
		fabric-tag-extensions-v0: Fabric Tag Extensions (v0) 0.2.5+5a6e8f4c45
		fabric-textures-v0: Fabric Textures (v0) 1.0.4+eae12eb845
		fabric-tool-attribute-api-v1: Fabric Tool Attribute API (v1) 1.1.4+5794386e7c
		fabricloader: Fabric Loader 0.8.8+build.202
		jankson: Jankson 3.0.0+j1.2.0
		libgui: LibGui 2.0.1+1.16.1
		minecraft: Minecraft 1.16.1
		modmenu: Mod Menu 1.10.2+build.32
	Launched Version: Fabric
	Backend library: LWJGL version 3.2.2 build 10
	Backend API: NO CONTEXT
	GL Caps: 
	Using VBOs: Yes
	Is Modded: Definitely; Client brand changed to 'fabric'
	Type: Client (map_client.txt)
	CPU: <unknown>