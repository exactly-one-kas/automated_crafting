---- Minecraft Crash Report ----
// There are four lights!

Time: 6/7/20 11:30 PM
Description: mouseClicked event handler

java.lang.NoClassDefFoundError: me/shedaniel/clothconfig2/api/ConfigEntryBuilder
	at me.sargunvohra.mcmods.autoconfig1u.gui.DefaultGuiProviders.<clinit>(DefaultGuiProviders.java:33)
	at me.sargunvohra.mcmods.autoconfig1u.AutoConfig$ClientOnly.<clinit>(AutoConfig.java:81)
	at me.sargunvohra.mcmods.autoconfig1u.AutoConfig.getConfigScreen(AutoConfig.java:71)
	at AutomatedCraftingModMenuIntegration.getConfigScreen(AutomatedCraftingModMenuIntegration.java:34)
	at AutomatedCraftingModMenuIntegration.lambda$getModConfigScreenFactory$0(AutomatedCraftingModMenuIntegration.java:23)
	at io.github.prospector.modmenu.ModMenu.getConfigScreen(ModMenu.java:44)
	at io.github.prospector.modmenu.gui.ModsScreen.lambda$init$1(ModsScreen.java:93)
	at net.minecraft.client.gui.widget.ButtonWidget.onPress(ButtonWidget.java:16)
	at net.minecraft.client.gui.widget.AbstractPressableButtonWidget.onClick(AbstractPressableButtonWidget.java:16)
	at net.minecraft.client.gui.widget.AbstractButtonWidget.mouseClicked(AbstractButtonWidget.java:140)
	at net.minecraft.client.gui.ParentElement.mouseClicked(ParentElement.java:42)
	at net.minecraft.client.Mouse.method_1611(Mouse.java:80)
	at net.minecraft.client.gui.screen.Screen.wrapScreenError(Screen.java:435)
	at net.minecraft.client.Mouse.onMouseButton(Mouse.java:80)
	at net.minecraft.client.Mouse.method_22686(Mouse.java:153)
	at net.minecraft.util.thread.ThreadExecutor.execute(ThreadExecutor.java:86)
	at net.minecraft.client.Mouse.method_22684(Mouse.java:153)
	at org.lwjgl.glfw.GLFWMouseButtonCallbackI.callback(GLFWMouseButtonCallbackI.java:36)
	at org.lwjgl.system.JNI.invokeV(Native Method)
	at org.lwjgl.glfw.GLFW.glfwPollEvents(GLFW.java:3101)
	at com.mojang.blaze3d.systems.RenderSystem.flipFrame(RenderSystem.java:85)
	at net.minecraft.client.util.Window.swapBuffers(Window.java:331)
	at net.minecraft.client.MinecraftClient.render(MinecraftClient.java:950)
	at net.minecraft.client.MinecraftClient.run(MinecraftClient.java:547)
	at net.minecraft.client.main.Main.main(Main.java:178)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at net.fabricmc.loader.game.MinecraftGameProvider.launch(MinecraftGameProvider.java:192)
	at net.fabricmc.loader.launch.knot.Knot.init(Knot.java:140)
	at net.fabricmc.loader.launch.knot.KnotClient.main(KnotClient.java:26)
	at net.fabricmc.devlaunchinjector.Main.main(Main.java:86)
Caused by: java.lang.ClassNotFoundException: me.shedaniel.clothconfig2.api.ConfigEntryBuilder
	at java.net.URLClassLoader.findClass(URLClassLoader.java:382)
	at java.lang.ClassLoader.loadClass(ClassLoader.java:418)
	at sun.misc.Launcher$AppClassLoader.loadClass(Launcher.java:355)
	at java.lang.ClassLoader.loadClass(ClassLoader.java:351)
	at net.fabricmc.loader.launch.knot.KnotClassLoader.loadClass(KnotClassLoader.java:161)
	at java.lang.ClassLoader.loadClass(ClassLoader.java:351)
	... 33 more


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- Head --
Thread: Render thread
Stacktrace:
	at me.sargunvohra.mcmods.autoconfig1u.gui.DefaultGuiProviders.<clinit>(DefaultGuiProviders.java:33)
	at me.sargunvohra.mcmods.autoconfig1u.AutoConfig$ClientOnly.<clinit>(AutoConfig.java:81)
	at me.sargunvohra.mcmods.autoconfig1u.AutoConfig.getConfigScreen(AutoConfig.java:71)
	at AutomatedCraftingModMenuIntegration.getConfigScreen(AutomatedCraftingModMenuIntegration.java:34)
	at AutomatedCraftingModMenuIntegration.lambda$getModConfigScreenFactory$0(AutomatedCraftingModMenuIntegration.java:23)
	at io.github.prospector.modmenu.ModMenu.getConfigScreen(ModMenu.java:44)
	at io.github.prospector.modmenu.gui.ModsScreen.lambda$init$1(ModsScreen.java:93)
	at net.minecraft.client.gui.widget.ButtonWidget.onPress(ButtonWidget.java:16)
	at net.minecraft.client.gui.widget.AbstractPressableButtonWidget.onClick(AbstractPressableButtonWidget.java:16)
	at net.minecraft.client.gui.widget.AbstractButtonWidget.mouseClicked(AbstractButtonWidget.java:140)
	at net.minecraft.client.gui.ParentElement.mouseClicked(ParentElement.java:42)
	at net.minecraft.client.Mouse.method_1611(Mouse.java:80)

-- Affected screen --
Details:
	Screen name: io.github.prospector.modmenu.gui.ModsScreen
Stacktrace:
	at net.minecraft.client.gui.screen.Screen.wrapScreenError(Screen.java:435)
	at net.minecraft.client.Mouse.onMouseButton(Mouse.java:80)
	at net.minecraft.client.Mouse.method_22686(Mouse.java:153)
	at net.minecraft.util.thread.ThreadExecutor.execute(ThreadExecutor.java:86)
	at net.minecraft.client.Mouse.method_22684(Mouse.java:153)
	at org.lwjgl.glfw.GLFWMouseButtonCallbackI.callback(GLFWMouseButtonCallbackI.java:36)
	at org.lwjgl.system.JNI.invokeV(Native Method)
	at org.lwjgl.glfw.GLFW.glfwPollEvents(GLFW.java:3101)
	at com.mojang.blaze3d.systems.RenderSystem.flipFrame(RenderSystem.java:85)

-- Affected level --
Details:
	All players: 1 total; [ClientPlayerEntity['Player67'/287, l='MpServer', x=-123.88, y=63.00, z=-255.68]]
	Chunk stats: Client Chunk Cache: 441, 289
	Level dimension: minecraft:overworld
	Level name: MpServer
	Level seed: -2161041525767650927
	Level generator: ID 00 - default, ver 1. Features enabled: false
	Level generator options: {}
	Level spawn location: World: (-128,63,-256), Chunk: (at 0,3,0 in -8,-16; contains blocks -128,0,-256 to -113,255,-241), Region: (-1,-1; contains chunks -32,-32 to -1,-1, blocks -512,0,-512 to -1,255,-1)
	Level time: 164716 game time, 1157 day time
	Known server brands: 
	Level was modded: false
	Level storage version: 0x00000 - Unknown?
	Level weather: Rain time: 0 (now: false), thunder time: 0 (now: false)
	Level game mode: Game mode: creative (ID 1). Hardcore: false. Cheats: false
	Server brand: fabric
	Server type: Integrated singleplayer server
Stacktrace:
	at net.minecraft.client.world.ClientWorld.addDetailsToCrashReport(ClientWorld.java:449)
	at net.minecraft.client.MinecraftClient.addDetailsToCrashReport(MinecraftClient.java:1839)
	at net.minecraft.client.MinecraftClient.run(MinecraftClient.java:561)
	at net.minecraft.client.main.Main.main(Main.java:178)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at net.fabricmc.loader.game.MinecraftGameProvider.launch(MinecraftGameProvider.java:192)
	at net.fabricmc.loader.launch.knot.Knot.init(Knot.java:140)
	at net.fabricmc.loader.launch.knot.KnotClient.main(KnotClient.java:26)
	at net.fabricmc.devlaunchinjector.Main.main(Main.java:86)

-- System Details --
Details:
	Minecraft Version: 1.15.2
	Minecraft Version ID: 1.15.2
	Operating System: Windows 10 (amd64) version 10.0
	Java Version: 1.8.0_251, Oracle Corporation
	Java VM Version: Java HotSpot(TM) 64-Bit Server VM (mixed mode), Oracle Corporation
	Memory: 366473856 bytes (349 MB) / 873463808 bytes (833 MB) up to 954728448 bytes (910 MB)
	CPUs: 16
	JVM Flags: 1 total; -Xmx1G
	Fabric Mods: 
		autoconfig1u: Auto Config v1 Updated 2.2.0
		automated_crafting: Example Mod 1.0.0
		fabric: Fabric API 0.5.1+build.294-1.15
		fabric-api-base: Fabric API Base 0.1.2+b7f9825d0c
		fabric-biomes-v1: Fabric Biomes (v1) 0.1.5+3b05f68e0c
		fabric-blockrenderlayer-v1: Fabric BlockRenderLayer Registration (v1) 1.1.4+c6a8ea890c
		fabric-commands-v0: Fabric Commands (v0) 0.1.2+b7f9825d0c
		fabric-containers-v0: Fabric Containers (v0) 0.1.3+b7f9825d0c
		fabric-content-registries-v0: Fabric Content Registries (v0) 0.1.3+b7f9825d0c
		fabric-crash-report-info-v1: Fabric Crash Report Info (v1) 0.1.2+b7f9825d0c
		fabric-dimensions-v1: fabric-dimensions-v1 0.3.0+2ad156310c
		fabric-events-interaction-v0: Fabric Events Interaction (v0) 0.3.0+fac69e320c
		fabric-events-lifecycle-v0: Fabric Events Lifecycle (v0) 0.1.2+b7f9825d0c
		fabric-item-groups-v0: Fabric Item Groups (v0) 0.1.6+ec40b2e10c
		fabric-keybindings-v0: Fabric Key Bindings (v0) 0.1.1+dfdb52d60c
		fabric-loot-tables-v1: Fabric Loot Tables (v1) 0.1.5+e08a73050c
		fabric-mining-levels-v0: Fabric Mining Levels (v0) 0.1.1+b7f9825d0c
		fabric-models-v0: Fabric Models (v0) 0.1.0+dfdb52d60c
		fabric-networking-blockentity-v0: Fabric Networking Block Entity (v0) 0.2.3+e08a73050c
		fabric-networking-v0: Fabric Networking (v0) 0.1.7+12515ed90c
		fabric-object-builders-v0: Fabric Object Builders (v0) 0.1.3+e4c9a9c30c
		fabric-particles-v1: fabric-particles-v1 0.1.1+dfdb52d60c
		fabric-registry-sync-v0: Fabric Registry Sync (v0) 0.2.6+f3d8141b0c
		fabric-renderer-api-v1: Fabric Renderer API (v1) 0.2.10+f08b61330c
		fabric-renderer-indigo: Fabric Renderer - Indigo 0.2.23+9290e2ed0c
		fabric-renderer-registries-v1: Fabric Renderer Registries (v1) 2.0.1+5a0f9a600c
		fabric-rendering-data-attachment-v1: Fabric Rendering Data Attachment (v1) 0.1.3+b7f9825d0c
		fabric-rendering-fluids-v1: Fabric Rendering Fluids (v1) 0.1.6+12515ed90c
		fabric-rendering-v0: Fabric Rendering (v0) 1.1.0+534104900c
		fabric-rendering-v1: Fabric Rendering (v1) 0.1.0+534104900c
		fabric-resource-loader-v0: Fabric Resource Loader (v0) 0.1.10+06c939b30c
		fabric-tag-extensions-v0: Fabric Tag Extensions (v0) 0.1.3+abd915800c
		fabric-textures-v0: Fabric Textures (v0) 1.0.4+821cdba70c
		fabricloader: Fabric Loader 0.8.2+build.194
		jankson: Jankson 2.0.1+j1.2.0
		libgui: LibGui 1.10.0+1.15.2
		minecraft: Minecraft 1.15.2
		modmenu: Mod Menu 1.10.2+build.32
	Launched Version: Fabric
	Backend library: LWJGL version 3.2.2 build 10
	Backend API: GeForce GTX 1080/PCIe/SSE2 GL version 4.6.0 NVIDIA 442.59, NVIDIA Corporation
	GL Caps: Using framebuffer using OpenGL 3.0
	Using VBOs: Yes
	Is Modded: Definitely; Client brand changed to 'fabric'
	Type: Client (map_client.txt)
	Resource Packs: 
	Current Language: English (US)
	CPU: 16x AMD Ryzen 7 1700 Eight-Core Processor 