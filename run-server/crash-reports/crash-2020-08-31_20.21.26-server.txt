---- Minecraft Crash Report ----
// Surprise! Haha. Well, this is awkward.

Time: 8/31/20 8:21 PM
Description: Exception while ticking

org.spongepowered.asm.mixin.transformer.throwables.IllegalClassLoadError: Illegal classload request for accessor mixin net.sssubtlety.automated_crafting.mixin.DefaultedListAccessor. The mixin is missing from automated_crafting.mixins.json which owns package net.sssubtlety.automated_crafting.mixin.* and the mixin has not been applied.
	at org.spongepowered.asm.mixin.transformer.MixinProcessor.applyMixins(MixinProcessor.java:330)
	at org.spongepowered.asm.mixin.transformer.MixinTransformer.transformClass(MixinTransformer.java:191)
	at org.spongepowered.asm.mixin.transformer.MixinTransformer.transformClassBytes(MixinTransformer.java:178)
	at org.spongepowered.asm.mixin.transformer.FabricMixinTransformerProxy.transformClassBytes(FabricMixinTransformerProxy.java:23)
	at net.fabricmc.loader.launch.knot.KnotClassDelegate.getPostMixinClassByteArray(KnotClassDelegate.java:157)
	at net.fabricmc.loader.launch.knot.KnotClassLoader.loadClass(KnotClassLoader.java:143)
	at java.lang.ClassLoader.loadClass(ClassLoader.java:351)
	at net.sssubtlety.automated_crafting.CraftingInventoryWithoutHandler.<init>(CraftingInventoryWithoutHandler.java:24)
	at net.sssubtlety.automated_crafting.blockEntity.AbstractAutoCrafterBlockEntity.getIsolatedInputInv(AbstractAutoCrafterBlockEntity.java:131)
	at net.sssubtlety.automated_crafting.blockEntity.SimpleAutoCrafterBlockEntity.optionalOutputCheck(SimpleAutoCrafterBlockEntity.java:28)
	at net.sssubtlety.automated_crafting.blockEntity.AbstractAutoCrafterBlockEntity.canOutput(AbstractAutoCrafterBlockEntity.java:109)
	at net.sssubtlety.automated_crafting.blockEntity.AbstractAutoCrafterBlockEntity.tryCraft(AbstractAutoCrafterBlockEntity.java:78)
	at net.sssubtlety.automated_crafting.block.AutoCrafterBlock.tryCraft(AutoCrafterBlock.java:54)
	at net.sssubtlety.automated_crafting.block.AutoCrafterBlock.scheduledTick(AutoCrafterBlock.java:94)
	at net.minecraft.block.AbstractBlock$AbstractBlockState.scheduledTick(AbstractBlock.java:618)
	at net.minecraft.server.world.ServerWorld.tickBlock(ServerWorld.java:592)
	at net.minecraft.server.world.ServerTickScheduler.tick(ServerTickScheduler.java:82)
	at net.minecraft.server.world.ServerWorld.tick(ServerWorld.java:328)
	at net.minecraft.server.MinecraftServer.tickWorlds(MinecraftServer.java:870)
	at net.minecraft.server.MinecraftServer.tick(MinecraftServer.java:808)
	at net.minecraft.server.integrated.IntegratedServer.tick(IntegratedServer.java:82)
	at net.minecraft.server.MinecraftServer.runServer(MinecraftServer.java:666)
	at net.minecraft.server.MinecraftServer.method_29739(MinecraftServer.java:226)
	at java.lang.Thread.run(Thread.java:748)


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- Head --
Thread: Server thread
Stacktrace:
	at org.spongepowered.asm.mixin.transformer.MixinProcessor.applyMixins(MixinProcessor.java:330)
	at org.spongepowered.asm.mixin.transformer.MixinTransformer.transformClass(MixinTransformer.java:191)
	at org.spongepowered.asm.mixin.transformer.MixinTransformer.transformClassBytes(MixinTransformer.java:178)
	at org.spongepowered.asm.mixin.transformer.FabricMixinTransformerProxy.transformClassBytes(FabricMixinTransformerProxy.java:23)
	at net.fabricmc.loader.launch.knot.KnotClassDelegate.getPostMixinClassByteArray(KnotClassDelegate.java:157)
	at net.fabricmc.loader.launch.knot.KnotClassLoader.loadClass(KnotClassLoader.java:143)
	at java.lang.ClassLoader.loadClass(ClassLoader.java:351)
	at net.sssubtlety.automated_crafting.CraftingInventoryWithoutHandler.<init>(CraftingInventoryWithoutHandler.java:24)
	at net.sssubtlety.automated_crafting.blockEntity.AbstractAutoCrafterBlockEntity.getIsolatedInputInv(AbstractAutoCrafterBlockEntity.java:131)
	at net.sssubtlety.automated_crafting.blockEntity.SimpleAutoCrafterBlockEntity.optionalOutputCheck(SimpleAutoCrafterBlockEntity.java:28)
	at net.sssubtlety.automated_crafting.blockEntity.AbstractAutoCrafterBlockEntity.canOutput(AbstractAutoCrafterBlockEntity.java:109)
	at net.sssubtlety.automated_crafting.blockEntity.AbstractAutoCrafterBlockEntity.tryCraft(AbstractAutoCrafterBlockEntity.java:78)
	at net.sssubtlety.automated_crafting.block.AutoCrafterBlock.tryCraft(AutoCrafterBlock.java:54)
	at net.sssubtlety.automated_crafting.block.AutoCrafterBlock.scheduledTick(AutoCrafterBlock.java:94)
	at net.minecraft.block.AbstractBlock$AbstractBlockState.scheduledTick(AbstractBlock.java:618)
	at net.minecraft.server.world.ServerWorld.tickBlock(ServerWorld.java:592)

-- Block being ticked --
Details:
	Block location: World: (183,72,241), Chunk: (at 7,4,1 in 11,15; contains blocks 176,0,240 to 191,255,255), Region: (0,0; contains chunks 0,0 to 31,31, blocks 0,0,0 to 511,255,511)
Stacktrace:
	at net.minecraft.server.world.ServerTickScheduler.tick(ServerTickScheduler.java:82)
	at net.minecraft.server.world.ServerWorld.tick(ServerWorld.java:328)

-- Affected level --
Details:
	All players: 1 total; [ServerPlayerEntity['Player779'/245, l='ServerLevel[1.16.1]', x=183.10, y=72.00, z=243.06]]
	Chunk stats: ServerChunkCache: 2025
	Level dimension: minecraft:overworld
	Level spawn location: World: (176,71,256), Chunk: (at 0,4,0 in 11,16; contains blocks 176,0,256 to 191,255,271), Region: (0,0; contains chunks 0,0 to 31,31, blocks 0,0,0 to 511,255,511)
	Level time: 189119 game time, 1000 day time
	Level name: 1.16.1
	Level game mode: Game mode: creative (ID 1). Hardcore: false. Cheats: true
	Level weather: Rain time: 93070 (now: false), thunder time: 39208 (now: false)
	Known server brands: fabric
	Level was modded: true
	Level storage version: 0x04ABD - Anvil
Stacktrace:
	at net.minecraft.server.MinecraftServer.tickWorlds(MinecraftServer.java:870)
	at net.minecraft.server.MinecraftServer.tick(MinecraftServer.java:808)
	at net.minecraft.server.integrated.IntegratedServer.tick(IntegratedServer.java:82)
	at net.minecraft.server.MinecraftServer.runServer(MinecraftServer.java:666)
	at net.minecraft.server.MinecraftServer.method_29739(MinecraftServer.java:226)
	at java.lang.Thread.run(Thread.java:748)

-- System Details --
Details:
	Minecraft Version: 1.16.1
	Minecraft Version ID: 1.16.1
	Operating System: Windows 10 (amd64) version 10.0
	Java Version: 1.8.0_251, Oracle Corporation
	Java VM Version: Java HotSpot(TM) 64-Bit Server VM (mixed mode), Oracle Corporation
	Memory: 483517920 bytes (461 MB) / 977272832 bytes (932 MB) up to 977272832 bytes (932 MB)
	CPUs: 16
	JVM Flags: 1 total; -Xmx1G
	Fabric Mods: 
		autoconfig1u: Auto Config v1 Updated 3.2.0-unstable
		automated_crafting: Automated Crafting 1.3.7+1.16.1
		cloth-basic-math: Cloth Basic Math 0.5.1
		cloth-config2: Cloth Config v4 4.6.0
		fabric: Fabric API 0.17.0+build.386-1.16.1
		fabric-api-base: Fabric API Base 0.1.3+12a8474c02
		fabric-biomes-v1: Fabric Biomes (v1) 0.2.7+059ea86602
		fabric-blockrenderlayer-v1: Fabric BlockRenderLayer Registration (v1) 1.1.4+c6a8ea8902
		fabric-command-api-v1: Fabric Command API (v1) 1.0.8+5ce5339802
		fabric-commands-v0: Fabric Commands (v0) 0.2.0+52d3083602
		fabric-containers-v0: Fabric Containers (v0) 0.1.8+045df74f02
		fabric-content-registries-v0: Fabric Content Registries (v0) 0.1.9+059ea86602
		fabric-crash-report-info-v1: Fabric Crash Report Info (v1) 0.1.2+b7f9825d02
		fabric-dimensions-v1: fabric-dimensions-v1 1.0.0+a71b305302
		fabric-events-interaction-v0: Fabric Events Interaction (v0) 0.3.3+7066030f02
		fabric-events-lifecycle-v0: Fabric Events Lifecycle (v0) 0.2.0+16acbe5b02
		fabric-game-rule-api-v1: Fabric Game Rule API (v1) 1.0.0+fe81e12502
		fabric-item-api-v1: Fabric Item API (v1) 1.0.0+16acbe5b02
		fabric-item-groups-v0: Fabric Item Groups (v0) 0.2.0+438f963602
		fabric-key-binding-api-v1: Fabric Key Binding API (v1) 1.0.1+f404f3be02
		fabric-keybindings-v0: Fabric Key Bindings (v0) 0.2.0+3fa9f7c502
		fabric-lifecycle-events-v1: Fabric Lifecycle Events (v1) 1.2.0+e83e061c02
		fabric-loot-tables-v1: Fabric Loot Tables (v1) 1.0.0+386eb69e02
		fabric-mining-levels-v0: Fabric Mining Levels (v0) 0.1.2+b764ce9902
		fabric-models-v0: Fabric Models (v0) 0.1.0+dfdb52d602
		fabric-networking-blockentity-v0: Fabric Networking Block Entity (v0) 0.2.5+b50ffc7b02
		fabric-networking-v0: Fabric Networking (v0) 0.1.10+e00ecb5f02
		fabric-object-builder-api-v1: Fabric Object Builder API (v1) 1.5.7+2242e77202
		fabric-object-builders-v0: Fabric Object Builders (v0) 0.6.1+a2d21ddd02
		fabric-particles-v1: fabric-particles-v1 0.2.1+0a6f2a7002
		fabric-registry-sync-v0: Fabric Registry Sync (v0) 0.3.8+7dba2d6c02
		fabric-renderer-api-v1: Fabric Renderer API (v1) 0.2.13+eae12eb802
		fabric-renderer-indigo: Fabric Renderer - Indigo 0.3.4+34d6c87102
		fabric-renderer-registries-v1: Fabric Renderer Registries (v1) 2.1.0+be551d3c02
		fabric-rendering-data-attachment-v1: Fabric Rendering Data Attachment (v1) 0.1.3+b7f9825d02
		fabric-rendering-fluids-v1: Fabric Rendering Fluids (v1) 0.1.8+b7084faa02
		fabric-rendering-v0: Fabric Rendering (v0) 1.1.0+5341049002
		fabric-rendering-v1: Fabric Rendering (v1) 1.1.2+346247d702
		fabric-resource-loader-v0: Fabric Resource Loader (v0) 0.2.6+f41e209802
		fabric-screen-handler-api-v1: Fabric Screen Handler API (v1) 1.1.0+97324d1102
		fabric-tag-extensions-v0: Fabric Tag Extensions (v0) 0.2.7+a4c57d8e02
		fabric-textures-v0: Fabric Textures (v0) 1.0.4+eae12eb802
		fabric-tool-attribute-api-v1: Fabric Tool Attribute API (v1) 1.2.1+91fca5cd02
		fabricloader: Fabric Loader 0.9.2+build.206
		jankson: Jankson 3.0.0+j1.2.0
		libgui: LibGui 2.3.0+1.16.1
		minecraft: Minecraft 1.16.1
		modmenu: Mod Menu 1.14.5+build.30
	Player Count: 1 / 8; [ServerPlayerEntity['Player779'/245, l='ServerLevel[1.16.1]', x=183.10, y=72.00, z=243.06]]
	Data Packs: vanilla, fabric/fabric-tool-attribute-api-v1, fabric/automated_crafting
	Type: Integrated Server (map_client.txt)
	Is Modded: Definitely; Client brand changed to 'fabric'