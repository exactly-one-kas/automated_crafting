---- Minecraft Crash Report ----
// You should try our sister game, Minceraft!

Time: 7/28/20 4:26 PM
Description: Exception while ticking

java.lang.ClassCastException: net.sssubtlety.automated_crafting.blockEntity.SimpleAutoCrafterBlockEntity cannot be cast to net.sssubtlety.automated_crafting.mixin.CraftingInventoryAccessor
	at net.sssubtlety.automated_crafting.blockEntity.AbstractAutoCrafterBlockEntity.internalGetStack(AbstractAutoCrafterBlockEntity.java:144)
	at net.sssubtlety.automated_crafting.blockEntity.AbstractAutoCrafterBlockEntity.tryCraft(AbstractAutoCrafterBlockEntity.java:79)
	at net.sssubtlety.automated_crafting.block.AutoCrafterBlock.scheduledTick(AutoCrafterBlock.java:120)
	at net.minecraft.block.AbstractBlock$AbstractBlockState.scheduledTick(AbstractBlock.java:618)
	at net.minecraft.server.world.ServerWorld.tickBlock(ServerWorld.java:592)
	at net.minecraft.server.world.ServerTickScheduler.tick(ServerTickScheduler.java:82)
	at net.minecraft.server.world.ServerWorld.tick(ServerWorld.java:328)
	at net.minecraft.server.MinecraftServer.tickWorlds(MinecraftServer.java:870)
	at net.minecraft.server.MinecraftServer.tick(MinecraftServer.java:808)
	at net.minecraft.server.integrated.IntegratedServer.tick(IntegratedServer.java:82)
	at net.minecraft.server.MinecraftServer.method_29741(MinecraftServer.java:666)
	at net.minecraft.server.MinecraftServer.method_29739(MinecraftServer.java:226)
	at java.lang.Thread.run(Thread.java:748)


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- Head --
Thread: Server thread
Stacktrace:
	at net.sssubtlety.automated_crafting.blockEntity.AbstractAutoCrafterBlockEntity.internalGetStack(AbstractAutoCrafterBlockEntity.java:144)
	at net.sssubtlety.automated_crafting.blockEntity.AbstractAutoCrafterBlockEntity.tryCraft(AbstractAutoCrafterBlockEntity.java:79)
	at net.sssubtlety.automated_crafting.block.AutoCrafterBlock.scheduledTick(AutoCrafterBlock.java:120)
	at net.minecraft.block.AbstractBlock$AbstractBlockState.scheduledTick(AbstractBlock.java:618)
	at net.minecraft.server.world.ServerWorld.tickBlock(ServerWorld.java:592)

-- Block being ticked --
Details:
	Block location: World: (167,74,235), Chunk: (at 7,4,11 in 10,14; contains blocks 160,0,224 to 175,255,239), Region: (0,0; contains chunks 0,0 to 31,31, blocks 0,0,0 to 511,255,511)
Stacktrace:
	at net.minecraft.server.world.ServerTickScheduler.tick(ServerTickScheduler.java:82)
	at net.minecraft.server.world.ServerWorld.tick(ServerWorld.java:328)

-- Affected level --
Details:
	All players: 1 total; [ServerPlayerEntity['Player272'/245, l='ServerLevel[1.16.1]', x=171.46, y=73.00, z=233.69]]
	Chunk stats: ServerChunkCache: 2025
	Level dimension: minecraft:overworld
	Level spawn location: World: (176,71,256), Chunk: (at 0,4,0 in 11,16; contains blocks 176,0,256 to 191,255,271), Region: (0,0; contains chunks 0,0 to 31,31, blocks 0,0,0 to 511,255,511)
	Level time: 101709 game time, 1000 day time
	Level name: 1.16.1
	Level game mode: Game mode: creative (ID 1). Hardcore: false. Cheats: true
	Level weather: Rain time: 93070 (now: false), thunder time: 39208 (now: false)
	Known server brands: fabric
	Level was modded: true
	Level storage version: 0x04ABD - Anvil
Stacktrace:
	at net.minecraft.server.MinecraftServer.tickWorlds(MinecraftServer.java:870)
	at net.minecraft.server.MinecraftServer.tick(MinecraftServer.java:808)
	at net.minecraft.server.integrated.IntegratedServer.tick(IntegratedServer.java:82)
	at net.minecraft.server.MinecraftServer.method_29741(MinecraftServer.java:666)
	at net.minecraft.server.MinecraftServer.method_29739(MinecraftServer.java:226)
	at java.lang.Thread.run(Thread.java:748)

-- System Details --
Details:
	Minecraft Version: 1.16.1
	Minecraft Version ID: 1.16.1
	Operating System: Windows 10 (amd64) version 10.0
	Java Version: 1.8.0_251, Oracle Corporation
	Java VM Version: Java HotSpot(TM) 64-Bit Server VM (mixed mode), Oracle Corporation
	Memory: 250640384 bytes (239 MB) / 829947904 bytes (791 MB) up to 954728448 bytes (910 MB)
	CPUs: 16
	JVM Flags: 1 total; -Xmx1G
	Fabric Mods: 
		autoconfig1u: Auto Config v1 Updated 3.2.0-unstable
		automated_crafting: Automated Crafting 1.1.3+1.16.1
		cloth-basic-math: Cloth Basic Math 0.5.1
		cloth-config2: Cloth Config v4 4.5.6
		fabric: Fabric API 0.14.1+build.372-1.16
		fabric-api-base: Fabric API Base 0.1.3+12a8474c45
		fabric-biomes-v1: Fabric Biomes (v1) 0.2.7+059ea86645
		fabric-blockrenderlayer-v1: Fabric BlockRenderLayer Registration (v1) 1.1.4+c6a8ea8945
		fabric-command-api-v1: Fabric Command API (v1) 1.0.8+5ce5339802
		fabric-commands-v0: Fabric Commands (v0) 0.2.0+52d3083645
		fabric-containers-v0: Fabric Containers (v0) 0.1.8+045df74f45
		fabric-content-registries-v0: Fabric Content Registries (v0) 0.1.9+059ea86645
		fabric-crash-report-info-v1: Fabric Crash Report Info (v1) 0.1.2+b7f9825d45
		fabric-dimensions-v1: fabric-dimensions-v1 1.0.0+a71b305345
		fabric-events-interaction-v0: Fabric Events Interaction (v0) 0.3.3+7066030f45
		fabric-events-lifecycle-v0: Fabric Events Lifecycle (v0) 0.2.0+16acbe5b02
		fabric-item-api-v1: Fabric Item API (v1) 1.0.0+16acbe5b02
		fabric-item-groups-v0: Fabric Item Groups (v0) 0.2.0+438f963645
		fabric-key-binding-api-v1: Fabric Key Binding API (v1) 1.0.0+e16a977445
		fabric-keybindings-v0: Fabric Key Bindings (v0) 0.2.0+3fa9f7c545
		fabric-lifecycle-events-v1: Fabric Lifecycle Events (v1) 1.0.0+b0993bc102
		fabric-loot-tables-v1: Fabric Loot Tables (v1) 0.1.10+059ea86645
		fabric-mining-levels-v0: Fabric Mining Levels (v0) 0.1.2+b764ce9945
		fabric-models-v0: Fabric Models (v0) 0.1.0+dfdb52d645
		fabric-networking-blockentity-v0: Fabric Networking Block Entity (v0) 0.2.5+b50ffc7b45
		fabric-networking-v0: Fabric Networking (v0) 0.1.10+e00ecb5f02
		fabric-object-builder-api-v1: Fabric Object Builder API (v1) 1.5.5+e00ecb5f02
		fabric-object-builders-v0: Fabric Object Builders (v0) 0.6.0+da175ad645
		fabric-particles-v1: fabric-particles-v1 0.2.1+0a6f2a7002
		fabric-registry-sync-v0: Fabric Registry Sync (v0) 0.3.8+7dba2d6c45
		fabric-renderer-api-v1: Fabric Renderer API (v1) 0.2.13+eae12eb845
		fabric-renderer-indigo: Fabric Renderer - Indigo 0.3.2+4d66bed502
		fabric-renderer-registries-v1: Fabric Renderer Registries (v1) 2.0.1+5a0f9a6045
		fabric-rendering-data-attachment-v1: Fabric Rendering Data Attachment (v1) 0.1.3+b7f9825d45
		fabric-rendering-fluids-v1: Fabric Rendering Fluids (v1) 0.1.8+b7084faa02
		fabric-rendering-v0: Fabric Rendering (v0) 1.1.0+5341049045
		fabric-rendering-v1: Fabric Rendering (v1) 1.1.2+346247d745
		fabric-resource-loader-v0: Fabric Resource Loader (v0) 0.2.6+f41e209802
		fabric-screen-handler-api-v1: Fabric Screen Handler API (v1) 1.0.1+f362c86e02
		fabric-tag-extensions-v0: Fabric Tag Extensions (v0) 0.2.7+a4c57d8e02
		fabric-textures-v0: Fabric Textures (v0) 1.0.4+eae12eb845
		fabric-tool-attribute-api-v1: Fabric Tool Attribute API (v1) 1.1.4+5794386e02
		fabricloader: Fabric Loader 0.8.8+build.202
		jankson: Jankson 3.0.0+j1.2.0
		libgui: LibGui 2.0.1+1.16.1
		minecraft: Minecraft 1.16.1
		modmenu: Mod Menu 1.12.2+build.17
	Player Count: 1 / 8; [ServerPlayerEntity['Player272'/245, l='ServerLevel[1.16.1]', x=171.46, y=73.00, z=233.69]]
	Data Packs: vanilla, fabric/fabric-tool-attribute-api-v1, fabric/automated_crafting
	Type: Integrated Server (map_client.txt)
	Is Modded: Definitely; Client brand changed to 'fabric'